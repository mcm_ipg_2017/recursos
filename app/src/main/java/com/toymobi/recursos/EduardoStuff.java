package com.toymobi.recursos;

import java.util.Locale;
import java.util.Random;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;

import com.toymobi.framework.dialog.CustomBigDialog;
import com.toymobi.framework.dialog.CustomDialog;
import com.toymobi.framework.dimensions.DeviceDimensionsUtil;
import com.toymobi.framework.locale.LocaleSetting;
import com.toymobi.framework.options.GlobalSettings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import static com.toymobi.framework.options.GlobalSettings.LANGUAGE_BR;
import static com.toymobi.framework.options.GlobalSettings.LANGUAGE_EN_DEFAULT;
import static com.toymobi.framework.options.GlobalSettings.LANGUAGE_PT_DEFAULT;

public class EduardoStuff {

    public static boolean isMainMenu = true;

    private static final String PERSISTENCE_KEY = "macarena";

    public static boolean ENABLE_TTS = true;

    public static boolean ENABLE_SAVE_PAGE_BOOK = false;

    public static Random random = new Random();

    public static void showMessageExitSPC(final Context context) {

        if (context != null) {

            final OnClickListener yesConfirmation = new OnClickListener() {
                @Override
                public void onClick(final View view) {
                    exit(context);
                }
            };

            final OnClickListener noConfirmation = new OnClickListener() {
                @Override
                public void onClick(final View view) {

                }
            };

            CustomDialog.openDialogConfirmation(context,
                    R.layout.dialog_exit_layout_spc,
                    yesConfirmation,
                    noConfirmation,
                    R.id.confirmation_button_yes,
                    R.id.confirmation_button_no);
        }
    }

    public static void showMessageBackMainMenuSPC(final Context context) {

        if (context != null) {

            final OnClickListener yesConfirmation = new OnClickListener() {
                @Override
                public void onClick(final View view) {
                    goHome(context);
                }
            };

            final OnClickListener noConfirmation = new OnClickListener() {
                @Override
                public void onClick(final View view) {

                }
            };

            final CharSequence title = context.getText(R.string.back_message);

            CustomDialog.openDialogConfirmation(context,
                    R.layout.dialog_back_layout_spc,
                    yesConfirmation,
                    noConfirmation,
                    R.id.confirmation_button_yes,
                    R.id.confirmation_button_no,
                    title,
                    R.id.dialogTitle);
        }
    }

    private static void exit(final Context context) {
        final AppCompatActivity actionBarActivity = ((AppCompatActivity) context);
        if (actionBarActivity != null) {
            actionBarActivity.finish();
        }
    }

    private static void goHome(final Context context) {

        final AppCompatActivity actionBarActivity = ((AppCompatActivity) context);

        if (actionBarActivity != null) {

            final FragmentManager fragmentManager = actionBarActivity.getSupportFragmentManager();
            fragmentManager.popBackStack();

        }
    }

    public static void showInfoOld(final Context context,
                                   CharSequence infoText) {

        if (context != null) {
            if (infoText != null && infoText.length() > 0) {
                CustomBigDialog.openDialogInformationJustify(context,
                        R.layout.dialog_info_layout_default,
                        R.id.textview_text_info, R.id.confirmation_button_yes,
                        infoText);
            }
        }
    }

    public static void showInfoNew(final Context context,
                                   CharSequence infoText) {

        if (context != null) {
            if (infoText != null && infoText.length() > 0) {
                CustomBigDialog.openDialogInformationJustify(context,
                        R.layout.info_dialog_layout,
                        R.id.textview_text_info, R.id.confirmation_button_yes,
                        infoText);
            }
        }
    }

    public static void showCongratsDialog(final Context context,
                                          final CharSequence infoText,
                                          final OnClickListener ok) {

        if (context != null) {
            if (infoText != null && infoText.length() > 0) {

                CustomDialog.openDialogInformation(context,
                        R.layout.dialog_congrats_layout, infoText,
                        R.id.dialogTitle, R.id.confirmation_button, ok);
            }
        }
    }

    public static void setLanguage(final Context context) {

        if (context != null) {

            GlobalSettings.languageOption = LocaleSetting.getLocalePersistence(
                    context, PERSISTENCE_KEY);

            Locale locale;

            switch (GlobalSettings.languageOption) {

                case LANGUAGE_PT_DEFAULT:

                    locale = new Locale(LANGUAGE_PT_DEFAULT);

                    LocaleSetting.changeLocale(context, locale, PERSISTENCE_KEY);

                    LocaleSetting.setLocalePersistence(context, PERSISTENCE_KEY, LANGUAGE_PT_DEFAULT);

                    break;

                case LANGUAGE_BR:

                    locale = new Locale("pt", "BR");

                    LocaleSetting.changeLocale(context, locale, PERSISTENCE_KEY);

                    LocaleSetting.setLocalePersistence(context, PERSISTENCE_KEY, LANGUAGE_BR);

                    break;

                case LANGUAGE_EN_DEFAULT:

                    break;

                default:

                    locale = new Locale(GlobalSettings.LANGUAGE_EN_DEFAULT);

                    LocaleSetting.changeLocale(context, locale, PERSISTENCE_KEY);

                    LocaleSetting.setLocalePersistence(context, PERSISTENCE_KEY, LANGUAGE_EN_DEFAULT);

                    break;
            }
        }
    }

    public static boolean isNormalSize(final Context context) {
        if (context != null) {
            final int density = DeviceDimensionsUtil.getDensity(context);
            return density <= DisplayMetrics.DENSITY_HIGH;
        }
        return true;
    }
}
