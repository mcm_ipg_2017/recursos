package com.toymobi.recursos;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.toymobi.framework.debug.DebugUtility;
import com.toymobi.framework.feedback.VibrationFeedback;
import com.toymobi.framework.media.MediaPlayerControl;
import com.toymobi.framework.media.MusicPlayer;
import com.toymobi.framework.media.SimpleSoundPool;
import com.toymobi.framework.options.GlobalSettings;
import com.toymobi.framework.raw.GetTextRawFile;
import com.toymobi.framework.view.textview.TextViewNotoBold;


public abstract class BaseFragment extends Fragment {

    private static final int RESOURCE_NO_CREATE = -1;

    public static boolean startSingleApp = false;

    private MediaPlayerControl musicPlayer;

    private SimpleSoundPool sfx;

    private VibrationFeedback vibrationFeedback;

    public MenuItem itemSound;

    public View mainView;

    @Override
    public void onStart() {
        super.onStart();

        if (musicPlayer != null)
            musicPlayer.loadMedia(GlobalSettings.soundEnable);
    }

    @Override
    public void onResume() {
        DebugUtility.printDebug("BaseFragment onResume");
        super.onResume();
        if (GlobalSettings.soundEnable && musicPlayer != null) {
            musicPlayer.play();
        }
        //Bug madito do fragment que nao carrega a UI apos power down ou rotate
        this.requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onPause() {
        DebugUtility.printDebug("BaseFragment onPause");
        super.onPause();

        if (GlobalSettings.soundEnable && musicPlayer != null) {
            musicPlayer.pause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (musicPlayer != null) {
            musicPlayer.release();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        deallocate();
    }

    public final void createToolBar(final int idToolbar, final int idTitle) {

        DebugUtility.printDebug("BaseFragment createToolBar");

        if (mainView != null) {

            setHasOptionsMenu(true);

            final Toolbar toolbar = mainView.findViewById(idToolbar);

            final TextViewNotoBold title = toolbar.findViewById(R.id.toolbar_title);

            final CharSequence titleChar = getText(idTitle);

            title.setText(titleChar);

            final AppCompatActivity activity = (AppCompatActivity) getActivity();

            if (activity != null) {
                activity.setSupportActionBar(toolbar);
            }
        }
    }

    public Toolbar getToolBar(final int idToolbar) {

        Toolbar toolbar = null;

        if (mainView != null) {
            toolbar = mainView.findViewById(idToolbar);
        }
        return toolbar;
    }


    /*
     * Criando o metodo para musica de background
     */
    public final void createMusicBackground(final int musicNameId, int musicRawID, boolean isLooping) {

        DebugUtility.printDebug("BaseFragment createMusicBackground");

        if (musicPlayer == null) {

            final Context context = getContext();

            if (context != null) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    musicPlayer = new MusicPlayer(context, musicRawID, isLooping);

                } else {

                    final String packageName = context.getPackageName();

                    final String musicPath = context.getString(musicNameId);

                    final String music = GetTextRawFile.getRawSoundsFileFixedPathPackage(packageName, musicPath);

                    musicPlayer = new MusicPlayer(context, music, isLooping);
                }
            }
        }
    }

    public final void createSFX(final int sfxId) {
        if (sfx == null) {

            final Context context = getContext();

            if (context != null) {
                sfx = new SimpleSoundPool(context, sfxId);
            }
        }
    }

    public final void playSFX(final int sfxId) {

        if (sfx == null) {
            createSFX(sfxId);
        }

        if (GlobalSettings.soundEnable) {
            sfx.playSound(sfxId);
        }
    }

    public void deallocate() {

        if (musicPlayer != null) {
            musicPlayer.release();
            musicPlayer = null;
        }

        if (sfx != null) {
            sfx.release();
            sfx = null;
        }

        vibrationFeedback = null;

        final FragmentManager fragmentManager = getFragmentManager();

        if (fragmentManager != null) {
            final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(this);
        }
    }

    public final void createVibrationeedback() {
        if (vibrationFeedback == null) {
            vibrationFeedback = new VibrationFeedback(getActivity());
        }
    }

    public final void playVibrationFeedback() {
        if (vibrationFeedback != null) {
            vibrationFeedback.vibrate();
        }
    }

    public final void playFeedBackButtons() {
        playVibrationFeedback();
        playSFX(R.raw.sfx_normal_click);
    }

    public final void playSfxFeedBackButtons() {
        playSFX(R.raw.sfx_normal_click);
    }

    public final void exitOrBackMenu(final boolean startSingleApp) {

        final Activity activityCompat = getActivity();

        if (startSingleApp) {
            EduardoStuff.showMessageExitSPC(activityCompat);
        } else {
            EduardoStuff.showMessageBackMainMenuSPC(activityCompat);
        }
    }

    public final void changeSoundMenu() {

        GlobalSettings.soundEnable ^= true;

        setIconSoundMenu();

        if (GlobalSettings.soundEnable) {
            musicPlayer.play();
        } else {
            musicPlayer.pause();
        }
    }

    public final void setIconSoundMenu() {
        if (itemSound != null) {
            if (GlobalSettings.soundEnable) {
                itemSound.setIcon(R.drawable.icon_sound_on);
            } else {
                itemSound.setIcon(R.drawable.icon_sound_off);
            }
        }
    }
}
